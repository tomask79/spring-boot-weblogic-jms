package com.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;

/**
 * Created by tomask79 on 26.02.17.
 */
@Configuration
public class WebLogicConnectorUtils {

    @Bean
    public InitialContext webLogicInitialContextExt() {
        System.out.println("Initializing WL context!");

        Hashtable<String, String> h = new Hashtable<String, String>(7);
        h.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        h.put(Context.PROVIDER_URL, "t3://localhost:7001");
        h.put(Context.SECURITY_PRINCIPAL, "weblogic");
        h.put(Context.SECURITY_CREDENTIALS, "weblogic123");

        InitialContext ctx = null;

        try {
            ctx = new InitialContext(h);
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return ctx;
    }

    @Bean
    public Context webLogicInitialContextServerSide() throws NamingException{
        return new InitialContext();
    }
}
