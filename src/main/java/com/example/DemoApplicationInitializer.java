package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.WebApplicationInitializer;

/**
 * Created by tomask79 on 26.02.17.
 */
@SpringBootApplication
@EnableScheduling
public class DemoApplicationInitializer extends SpringBootServletInitializer implements WebApplicationInitializer{
    private static Class<DemoApplicationInitializer> applicationClass = DemoApplicationInitializer.class;

    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplicationInitializer.class, args);
    }
}
