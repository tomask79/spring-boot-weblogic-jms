package com.example.jms;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.jms.*;
import javax.naming.Context;
import javax.naming.NamingException;
import java.util.Date;

/**
 * Created by tomask79 on 26.02.17.
 */
@Service
public class JMSUtils {
    @Resource(name="webLogicInitialContextServerSide")
    public Context ctx;

    // Defines the JMS context factory.
    public final static String JMS_FACTORY="jms.hs.ConnectionFactory";

    // Defines the queue.
    public final static String QUEUE="jms.hs.Test";

    private QueueConnectionFactory qconFactory;
    private QueueConnection qcon;
    private QueueSession qsession;
    private QueueSender qsender;
    private Queue queue;
    private TextMessage msg;

    @Scheduled(fixedRate = 1000)
    public void sendIntoJMSQueue() throws Exception{
        try {
            System.out.println("Initializing...");
            init(ctx, QUEUE);
            Date currentTime = new Date();
            System.out.println(currentTime);
            send(currentTime.toString());
        } finally {
            close();
        }
    }

    public void init(Context ctx, String queueName)
            throws NamingException, JMSException
    {
        qconFactory = (QueueConnectionFactory) ctx.lookup(JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = (Queue) ctx.lookup(queueName);
        qsender = qsession.createSender(queue);
        msg = qsession.createTextMessage();
        qcon.start();
    }

    /**
     * Sends a message to a JMS queue.
     *
     * @param message  message to be sent
     * @exception JMSException if JMS fails to send message due to internal error
     */
    public void send(String message) throws JMSException {
        msg.setText(message);
        qsender.send(msg);
    }

    /**
     * Closes JMS objects.
     * @exception JMSException if JMS fails to close objects due to internal error
     */
    public void close() throws JMSException {
        qsender.close();
        qsession.close();
        qcon.close();
    }
}
