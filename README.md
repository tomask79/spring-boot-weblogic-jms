# Spring Boot and WebLogic 12c #

When running Spring Boot application against WebLogic 12c, you've got two options:

* **Connecting to WebLogic externally** and running Spring Boot app standalone at Tomcat or other container
* Deploy your Spring Boot app **directly to WebLogic**

## InitialContext and connecting to WebLogic 12c externally ##

```
    @Bean
    public InitialContext webLogicInitialContextExt() {
        System.out.println("Initializing WL context!");

        Hashtable<String, String> h = new Hashtable<String, String>(7);
        h.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        h.put(Context.PROVIDER_URL, "t3://localhost:7001");
        h.put(Context.SECURITY_PRINCIPAL, "weblogic");
        h.put(Context.SECURITY_CREDENTIALS, "weblogic123");

        InitialContext ctx = null;

        try {
            ctx = new InitialContext(h);
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return ctx;
    }
```
Code is self explanatory more or less. To run it you just **need to install weblogic client jar** as maven dependency to avoid ClassNotFoundException with "weblogic.jndi.WLInitialContextFactory". Simply refer to your Oracle WebLogic installation's server/lib folder and look for **wlthint3client.jar** which is a minimal jar containing minimal list of necessary classes to connect to WLS and getting some JNDI stuff. I highly recommend to **install it through maven** to avoid headache problems.

```
mvn install:install-file -Dfile=<path-to-file> -DgroupId=<group-id> \
    -DartifactId=<artifact-id> -Dversion=<version> -Dpackaging=<packaging>
```
## InitialContext and Spring Boot running inside of WebLogic 12c ##

If you run your Spring Boot app inside of WebLogic then initializing of Context is more more simplier:

```
    @Bean
    public Context webLogicInitialContextServerSide() throws NamingException{
        return new InitialContext();
    }
```
You do not need to specify factory or URL. Server will use by default WLInitialContextFactory class and it's URL.

## Using the InitialContext to get server artifacts through JNDI ##

Lets create a simple application which will push JMS message per second into the JMS Queue. 

First refer to the server side created InitialContext:
(if you want InitialContext from external WebLogic just change the bean name to "webLogicInitialContextExt")
```
    @Resource(name="webLogicInitialContextServerSide")
    public Context ctx;
```

Whole sending class:
(configure your JMS stuff appropriatelly)
```
@Service
public class JMSUtils {
    @Resource(name="webLogicInitialContextServerSide")
    public Context ctx;

    // Defines the JMS context factory.
    public final static String JMS_FACTORY="jms.hs.ConnectionFactory";

    // Defines the queue.
    public final static String QUEUE="jms.hs.Test";

    private QueueConnectionFactory qconFactory;
    private QueueConnection qcon;
    private QueueSession qsession;
    private QueueSender qsender;
    private Queue queue;
    private TextMessage msg;

    @Scheduled(fixedRate = 1000)
    public void sendIntoJMSQueue() throws Exception{
        try {
            System.out.println("Initializing...");
            init(ctx, QUEUE);
            Date currentTime = new Date();
            System.out.println(currentTime);
            send(currentTime.toString());
        } finally {
            close();
        }
    }

    public void init(Context ctx, String queueName)
            throws NamingException, JMSException
    {
        qconFactory = (QueueConnectionFactory) ctx.lookup(JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = (Queue) ctx.lookup(queueName);
        qsender = qsession.createSender(queue);
        msg = qsession.createTextMessage();
        qcon.start();
    }

    /**
     * Sends a message to a JMS queue.
     *
     * @param message  message to be sent
     * @exception JMSException if JMS fails to send message due to internal error
     */
    public void send(String message) throws JMSException {
        msg.setText(message);
        qsender.send(msg);
    }

    /**
     * Closes JMS objects.
     * @exception JMSException if JMS fails to close objects due to internal error
     */
    public void close() throws JMSException {
        qsender.close();
        qsession.close();
        qcon.close();
    }
}
```

## Few gotchas when running Spring Boot app at WebLogic 12c ##

Your app needs to contain **weblogic.xml** descriptor in the **src/main/webapp/WEB-INF** folder. Other locations won't work. For other details, simply read [Spring-Boot deploy to WebLogic](http://docs.spring.io/spring-boot/docs/current/reference/html/howto-traditional-deployment.html#howto-weblogic). Also don't forget to set following dependency:


```
       <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<artifactId>tomcat-embed-el</artifactId>
					<groupId>org.apache.tomcat.embed</groupId>
				</exclusion>
			</exclusions>
		</dependency>
```
With this you're going to ensure that the embedded servlet container doesn’t interfere with the servlet container to which the war file will be deployed.

## Testing this repo ##

* Install Weblogic 12c
* Create there domain with JMS Queue "jms.hs.Test" and JMS Connection Factory "jms.hs.ConnectionFactory"
* git clone <this repo>
* Deploy WAR from the target folder to WebLogic and start it
* New messages should appear in the "jms.hs.Test" queue.

Best Regards

Tomas